var express = require('express');
var router = express.Router();
var DBconnect = require('../DBconnect.js');


router.get('/:user', async function(req, res){
   let results = await DBconnect.runQuery('select * from todos where user_id=$1 order by done', [req.params.user ])
   //console.log(results)
   res.send(results.rows)
});

router.post('/:text/:user', async function(req, res){
   var date=new Date()
   let results = await DBconnect.runQuery('INSERT INTO todos (item, done, user_id,created_date, updated_date) VALUES ($1, FALSE,$2,$3,$3)', [req.params.text, req.params.user,date])
   //console.log(results)
   res.send(results)

});

//used Delete. Even though doing a soft delete, i still felt it better to use the delete method than the put method.
router.delete('/:todo', async function(req, res){
   var date=new Date()
   let results = await DBconnect.runQuery('UPDATE todos set updated_date=$1, done= NOT done where id=$2', [date,req.params.todo])
   res.send(results)
});

//export this router to use in our index.js
module.exports = router;