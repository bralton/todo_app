//require the just installed express app
const express = require('express');
const todos = require('./routes/todos.js')

const app = express();

app.use((req, res, next) => {
    //this shouldnt be wildcarded, needs restricting.
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');

    next();
  });

app.use('/todos',todos)

//the server is listening on port 5000 for connections
app.listen(5000, function () {
  console.log('ToDo api listening on port 5000!')
});