const { Client } = require('pg')

const runQuery = async (query, params) => {
        let client = new Client({
            user: 'todo_app',
            host: 'localhost',
            database: 'todo_app',
            password: 'icouldntpickapassword',
            port: 5432,
        })
        const completeQuery = {
            text: query,
            values: params,
          }
        await client.connect()
        let results = await client.query(completeQuery)
        await client.end()
        //console.log(results)
        return results
    }

module.exports = { runQuery }