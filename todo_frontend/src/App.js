import React, { Component } from 'react';
import './App.css';
import Todo from './Todo';
import TodoList from './TodoList';
import request from 'request';

class App extends Component {
  inputElement = React.createRef()

  constructor() {
    super()
    this.state = {
      todos: [],
      newItem: '',
      user:''
    }
  }

  handleInput = e => {
    e.preventDefault()
    const newItem = e.target.value
    this.setState({
      newItem,
    })
  }

  addTodo = e => {
    e.preventDefault()
    fetch('http://localhost:5000/todos/' + this.state.newItem + '/' + this.state.user, { method:"POST"}).then(res => res.json()).then((body) => {
      console.log(body)
      this.getTodos(this.state.user)
     }).catch(err => console.log(err))
     this.setState({
      newItem :''
    })
  }

  deleteTodo = key => {
    fetch('http://localhost:5000/todos/' + key, { method:"DELETE"}).then(res => res.json()).then((body) => {
      console.log(body)
      this.getTodos(this.state.user)
     }).catch(err => console.log(err))

  }

  create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c==='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
  }

  getTodos(user) {
    fetch('http://localhost:5000/todos/'+ user, { method:"GET"}).then(res => res.json()).then((body) => {
      
      //console.log(body)
      this.setState({ todos: body })
     }).catch(err => console.log(err))
  }

  componentDidMount() {
     let cookie = document.cookie
     if(cookie===""){
       var date=new Date()
       date.setDate(date.getDate() + 7);
       //console.log(date)
       cookie="username=" + this.create_UUID() + ";expires " + date
       document.cookie=cookie
     }
    let username=cookie.split(';')[0]
    console.log(username)
    let user=username.split('=')[1]
    console.log(user)
    this.setState({user:user})
    this.getTodos(user)
     
  }

  render() {
    return (
        <div className="mainContainer">
        <Todo addTodo={this.addTodo}
          handleInput={this.handleInput}
          newItem={this.state.newItem}
          inputElement={this.inputElement} />
        <TodoList todos={this.state.todos}
          deleteTodo={this.deleteTodo} />
      </div>
    );
  }
}

export default App;
