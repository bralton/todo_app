import React, { Component } from 'react'

class TodoList extends Component {
    createTasks = item => {
      if(item.done){
        return (<li class="list-group-item" key={item.id} onClick={() => this.props.deleteTodo(item.id)}><del>
          {item.item}
          </del>
        </li> ) }
        else {return (
          <li class="list-group-item" key={item.id} onClick={() => this.props.deleteTodo(item.id)}>
          {item.item}
        </li> 
        )}
      }
      render() {
        const todos = this.props.todos
        const listOfItems = todos.map(this.createTasks)
        return (<div><h4>Click a ToDo to toggle its status.</h4> <ul class="list-group list-group-flush">{listOfItems}</ul> </div>)
      }
  }

export default TodoList