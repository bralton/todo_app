import React, { Component } from 'react'
class Todo extends Component {
  componentDidUpdate() {
    this.props.inputElement.current.focus()
  }
  render() {
    return (
      <div className="todoListMain">
        <div className="header">
          <h1>ToDo List App</h1>
          <form onSubmit={this.props.addTodo}>
          <div class="input-group mb-3">
            <input class="form-control"  placeholder="ToDo" ref={this.props.inputElement} onChange={this.props.handleInput} value={this.props.newItem}/>
            <div class="input-group-append"> <button class="btn btn-outline-secondary" type="submit"> Add Task </button> </div>        </div>

          </form>
        </div>
      </div>
    )
  }
}


export default Todo