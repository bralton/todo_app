CREATE TABLE todos (
	id serial PRIMARY KEY,
    item varchar NOT NULL,
    done boolean NOT NULL,
    user_id varchar NOT NULL,
    created_date timestamp NOT NULL,
    updated_date timestamp NOT NULL

);
